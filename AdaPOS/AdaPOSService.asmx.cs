﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using System.Text;

namespace AdaPOS
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AdaPOS : System.Web.Services.WebService
    {
        public enum UserStatus
        {
            userExists = 40 ,
            success = 1
        }

        private DataTable userexists(string num)
        {
            var conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["AdaPOS"].ConnectionString);
            conn.Open();
            string query = "select * from Users where PhoneNumber = @num ";
            var cmd = new SqlCommand(query, conn);
            var dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            var tbl = dt;
            string user = cmd.ExecuteScalar().ToString();
            return tbl;
        }

        private Boolean exists(string pnnno)
        {
            try
            {
                var Transacted = false;
                using (var dtlog = userexists(pnnno))
                {
                    if (dtlog.Rows.Count == 0)
                    {
                        Transacted = false;
                    }
                    else
                    {
                        Transacted = true;
                    }
                }
                return Transacted;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        [WebMethod]
        public int saveUser(string name, string phonenumber, string password)
        {
            
            var sqlconn = new SqlConnection(WebConfigurationManager.ConnectionStrings["AdaPOS"].ConnectionString);
                sqlconn.Open();
                string selectquery = " select * from users where PhoneNumber = @phonenumber";
                var cmd = new SqlCommand(selectquery, sqlconn);
                var exist = exists(phonenumber);
                var response = 0;
                //var response = UserStatus.userExists;
                switch (exist)
                {
                    case true:
                        response = 40;
                        break;
                    case false:
                         //save data  
                        var sqlcomm = new SqlCommand
                            {
                                Connection = sqlconn,
                                CommandType = CommandType.StoredProcedure,
                                CommandText = "Add_Users"
                            };
                            sqlcomm.Parameters.Add("@Id", SqlDbType.Decimal, 4).Direction = ParameterDirection.Output;
                            sqlcomm.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar)).Value = (name);
                            sqlcomm.Parameters.Add(new SqlParameter("@phoneNumber ", SqlDbType.VarChar)).Value = (phonenumber);
                            sqlcomm.Parameters.Add(new SqlParameter("@Password", SqlDbType.VarChar)).Value = (password);

                            sqlcomm.ExecuteNonQuery();
                            sqlcomm.Dispose();
                            sqlconn.Close();
                        //set response
                        response =   1;
                        break;
                        
                    default:
                        break;
                }

               

            return response;
        }

        [WebMethod]
        public DataTable getUser(string phoneNumber)
        {
            var conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["AdaPOS"].ConnectionString);
            conn.Open();
            string query = "select * from Users where PhoneNumber = @phoneNmuber ";
            var cmd = new SqlCommand(query, conn);
            var dt = new DataTable();
            string delim = ",";
            dt.Load(cmd.ExecuteReader());
            var tbl = dt;
            string user = cmd.ExecuteScalar().ToString();
            return tbl;
        }

        [WebMethod]
        public void saveInventory(string name, decimal costprice, decimal saleprice, string barcode)
        {
            try
            {
                var sqlconn = new SqlConnection(WebConfigurationManager.ConnectionStrings["AdaPOS"].ConnectionString);
                sqlconn.Open();
                var sqlcomm = new SqlCommand
                {
                    Connection = sqlconn,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "Add_Invetory"
                };
                sqlcomm.Parameters.Add("@Id", SqlDbType.Decimal, 4).Direction = ParameterDirection.Output;
                sqlcomm.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar)).Value = (name);
                sqlcomm.Parameters.Add(new SqlParameter("@SalePrice ", SqlDbType.VarChar)).Value = (saleprice);
                sqlcomm.Parameters.Add(new SqlParameter("@CostPrice", SqlDbType.VarChar)).Value = (costprice);
                sqlcomm.Parameters.Add(new SqlParameter("@BarCode", SqlDbType.VarChar)).Value = (barcode);

                sqlcomm.ExecuteNonQuery();
                sqlcomm.Dispose();
                sqlconn.Close();

            }
            catch (Exception ex)
            {


            }
        }

        [WebMethod]
        public DataTable getInventory(string barCode)
        {
            var conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["AdaPOS"].ConnectionString);
            conn.Open();
            string query = "select * from Invetory where Barcode = @barCode ";
            var cmd = new SqlCommand(query, conn);
            string inventory = cmd.ExecuteScalar().ToString();
            var dt = new DataTable();
            string delim = ",";
            dt.Load(cmd.ExecuteReader());
            var tbl = dt;
            string user = cmd.ExecuteScalar().ToString();
            
            return tbl;
        }

    }

}